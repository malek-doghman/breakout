# README #

### Break Out Game###

* (c++ / OpenGL 4.) , post process , blur , shake animation  
* BreakOut 1.0
* http://learnopengl.com/#!In-Practice/2D-Game/Breakout by JoeyDeVries 

### How do I get set up? ###

* Download and compile under Visual Studio 2013
* Dependencies : 

           OpenGL library         | GLEW     | https://github.com/nigels-com/glew
           ------------------------------------------------------------------------------
           Window Library         | GLFW 3   | http://www.glfw.org/
           ------------------------------------------------------------------------------
           Texture importer       | Soil     | https://www.lonesock.net/soil.html
           ------------------------------------------------------------------------------
           Math Library           | Glm      | https://glm.g-truc.net/0.9.8/index.html
           ------------------------------------------------------------------------------ 
           Object importer        | assimp   | https://github.com/assimp/assimp
           ------------------------------------------------------------------------------
           FreeType Library       | freetypes| https://www.freetype.org/
		   ------------------------------------------------------------------------------
           irrklang 1.5           | Sound    | https://www.ambiera.com/irrklang/

* Library included no need to install or downlaod anything just run !