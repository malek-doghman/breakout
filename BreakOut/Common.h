#pragma once
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <memory>
#include <vector>
#include <stdexcept>
#include <map>
#include <tuple>
#include <exception>
#include <algorithm>


#include "GL/glew.h"
#include "GLFW/glfw3.h"

//texture importer library
#include <SOIL.h>

//Math Library
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

//Object importer library
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

//free type library
#include <ft2build.h>
#include FT_FREETYPE_H  

#include <irrKlang.h>
using namespace irrklang;

enum Direction { UP, RIGHT, DOWN, LEFT };
typedef std::tuple<GLboolean, Direction, glm::vec2> Collision;

using namespace std; 