#include "TextRenderer.h"
#include "GLexception.h"

TextRenderer::TextRenderer(ShaderProgram& textShader , int faceWidth, int faceHeight)
{
	_faceWidth = faceWidth;
	_faceHeight = faceHeight;
	_textShader = textShader;

	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);
	glBindVertexArray(VAO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)* 6 * 4, NULL, GL_DYNAMIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

}

TextRenderer::~TextRenderer()
{
	FT_Done_Face(face);
	FT_Done_FreeType(ft);
}

void TextRenderer::InitFT(){
	if (FT_Init_FreeType(&ft))
		throw(glException(FTLoadError));
}

void TextRenderer::InitFace(){
	if (FT_New_Face(ft,&fontDirectory[0], 0, &face))
		throw(glException(FaceLoadError));
	FT_Set_Pixel_Sizes(face, _faceWidth, _faceHeight);
}

void TextRenderer::LoadChar(char glyph){
	if (FT_Load_Char(face, glyph, FT_LOAD_RENDER))
		throw(glException(GlyphLoadError));
}

void TextRenderer::LoadTextureChars(){

	Characters.get_allocator().allocate(128* (sizeof(GLchar)+sizeof(TextureChar)) );
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	for (GLubyte c = 0; c < 128; c++){
		LoadChar(c);
		TextureChar character;		
		character.Generate(face);
		Characters.insert(make_pair(c, character));
		cout << c << endl;
	}
}


void TextRenderer::Load(){
	InitFT();
	InitFace();
	LoadTextureChars();
}

void TextRenderer::RenderText(std::string text, GLfloat x, GLfloat y, GLfloat scale, glm::vec3 color){
	_textShader.use();
	_textShader.sendUniformData("textColor", color);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glActiveTexture(GL_TEXTURE0);
	glBindVertexArray(VAO);

	// Iterate through all characters
	std::string::const_iterator c;
	for (c = text.begin(); c != text.end(); c++)
	{
		TextureChar ch = Characters[*c];

		GLfloat xpos = x + ch._BearingX * scale;
		GLfloat ypos = y + (this->Characters['H']._BearingY - ch._BearingY) * scale;

		GLfloat w = ch.Width * scale;
		GLfloat h = ch.Height * scale;
		// Update VBO for each character
		GLfloat vertices[6][4] = {
			{ xpos, ypos + h, 0.0, 0.0 },
			{ xpos, ypos, 0.0, 1.0 },
			{ xpos + w, ypos, 1.0, 1.0 },

			{ xpos, ypos + h, 0.0, 0.0 },
			{ xpos + w, ypos, 1.0, 1.0 },
			{ xpos + w, ypos + h, 1.0, 0.0 }
		};
		// Render glyph texture over quad
		ch.Bind();
		// Update content of VBO memory
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		// Render quad
		glDrawArrays(GL_TRIANGLES, 0, 6);
		// Now advance cursors for next glyph (note that advance is number of 1/64 pixels)
		x += (ch._Advance >> 6) * scale; // Bitshift by 6 to get value in pixels (2^6 = 64)
	}
	glBindVertexArray(0);
	glBindTexture(GL_TEXTURE_2D, 0);

}

