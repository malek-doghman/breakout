#pragma once
#include "Common.h"
#include "GameObject.h"
#include "ResourcesManager.h"

class GameLevel
{
public:
	//OPTIMIZATION :DDD
	glm::vec2 tileSize;
	std::vector<std::vector<GLuint>> tileData;

	void resetLevel(GLuint levelWidth, GLuint levelHeight);
	// Level state
	std::vector<GameObject> Bricks;
	// Constructor
	GameLevel(){}
	GameLevel(const GLchar *file, GLuint levelWidth, GLuint levelHeight);
	// Loads level from file
	void      Load(const GLchar *file, GLuint levelWidth, GLuint levelHeight);
	// Render level
	void      Draw(SpriteRenderer &renderer);
	// Check if the level is completed (all non-solid tiles are destroyed)
	GLboolean IsCompleted();
private:
	// Initialize level from tile data
	void      init(GLuint levelWidth, GLuint levelHeight);
};
