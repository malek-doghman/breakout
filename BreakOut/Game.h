#pragma once 
#include "Common.h"
#include "ResourcesManager.h";
#include "SpriteRenderer.h"
#include "GameLevel.h"
#include "BallObject.h"
#include "ParticleGenerator.h"
#include "PostProcessor.h"
#include "PowerUp.h"
#include "TextRen.h"


enum GameState {GAME_ACTIVE, GAME_MENU ,GAME_WIN}; 

const float heightFactor = 0.70;
const glm::vec2 INITIAL_BALL_VELOCITY(150.0f, -400.0f);
const GLfloat BALL_RADIUS = 20.0f;
const glm::vec2 PADDLE_SIZE(150, 32);
const GLfloat PADDLE_VELOCITY(500.0f);
const GLuint NR_PARTICULE = 300; 
const GLuint NR_NEW_PARTICULE = 1;
const GLfloat SHAKE_TIME = 0.20f;

class Game
{
public:
	Game(int width , int height);
	~Game();
	void Init();
	void render();
	void ProcessInput(GLfloat dt, GLFWwindow* window );
	void Update(GLfloat dt);
private:
	vector<PowerUp> PowerUps;
	void UpdatePowerUps(GLfloat dt);
	void SpawnPowerUps(GameObject& box);

	GLboolean ShouldSpawn(GLuint chance);
	GLboolean isOtherPowerUpActive(std::vector<PowerUp> &powerUps, std::string type);

	GameState state; 
	GLuint width;
	GLuint height;
	vector<GameLevel> levels;
	GLuint currentLevel;
	//AABB - Circle collision detection
	//this algorithm is more precise than the last one 
	void BallCollision();
	void ResolveBrickCollision(Collision collision);
	void ResolvePaddleCollision(Collision collision);
	void ResetLevel();
};

