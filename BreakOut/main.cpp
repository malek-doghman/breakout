#include "Window.h"
#include "Game.h"

int width = 1280; 
int height = 720;
int samples = 4;

int main(){

	Window window("BreakOut", width, height);
	Game Breakout(width, height);

	try{		
		Breakout.Init();
	}
	catch (glException& e){
		cout << e.what() << endl;
		system("pause");
		exit(EXIT_FAILURE);
	}
	vector<float> backgroundColor = window.getBackGroundColor();

	GLfloat deltaTime = 0.0f;
	GLfloat lastFrame = 0.0f;

	//glViewport(0, 0, width/1.2, height);

	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	while (!glfwWindowShouldClose(window.GetWindow()))
	{
		GLfloat currentFrame = glfwGetTime();
		deltaTime = currentFrame - lastFrame;
		lastFrame = currentFrame;

		glClearBufferfv(GL_COLOR, 0, &backgroundColor[0]);

		Breakout.render();
		Breakout.ProcessInput(deltaTime, window.GetWindow());
		Breakout.Update(deltaTime);
		glfwPollEvents();
		glfwSwapBuffers(window.GetWindow());
	}

	ResourcesManager::Clear();
	glfwTerminate();

	return 0;
}