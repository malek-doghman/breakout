#include "Game.h"

SpriteRenderer *bkRenderer = nullptr;
SpriteRenderer *renderer = nullptr;
GameObject *paddle;
BallObject *Ball;
ParticleGenerator  *ParticlesGenerator;
PostProcessor   *Effects;
GLfloat ShakeTime = 0.0f;
TextRenderer* Text;
ISoundEngine *SoundEngine = createIrrKlangDevice();

Game::Game(int _width, int _height)
{
	width = _width;
	height = _height;
	state = GAME_ACTIVE;
}

Game::~Game()
{
	delete renderer;
	delete paddle;
	delete Ball;
	delete bkRenderer;
	delete ParticlesGenerator;
	delete Effects;
	delete Text;
	SoundEngine->drop();
}

void Game::Init(){
	//background shader
	ResourcesManager::LoadShader("Heart", "shader/quad.vert", "shader/backgroundShader/PurpleWaves.frag");
	//Loading shaders
	ResourcesManager::LoadShader("spriteShader", "shader/SpriteRenderer.vert", "shader/SpriteRenderer.frag");
	ResourcesManager::LoadShader("Particule", "shader/Particule.vert","shader/Particule.frag");
	ResourcesManager::LoadShader("PostProcess", "shader/PostProcessor.vert", "shader/PostProcessor.frag");
	ResourcesManager::LoadShader("textShader", "shader/Text.vert", "shader/Text.frag");
	//Setting uniform projection variable
	glm::mat4 projection = glm::ortho(0.0f, static_cast<GLfloat>(width), static_cast<GLfloat>(height), 0.0f, -1.0f, 1.0f);
	ResourcesManager::getShaderByName("spriteShader").use().sendUniformData("projection", projection);
	ResourcesManager::getShaderByName("Heart").use().sendUniformData("projection", projection);
	ResourcesManager::getShaderByName("Particule").use().sendUniformData("projection", projection);
	
	//glm::mat4 Tprojection = glm::ortho(0.0f, static_cast<GLfloat>(width), 0.0f, static_cast<GLfloat>(height));
	
	ResourcesManager::getShaderByName("textShader").use().sendUniformData("projection", glm::ortho(0.0f, static_cast<GLfloat>(width), static_cast<GLfloat>(height), 0.0f));
	ResourcesManager::getShaderByName("textShader").use().sendUniformData("text", 0);

	//Text renderer
	Text = new TextRenderer(this->width, this->height, ResourcesManager::getShaderByName("textShader"));
	Text->Load("font/ocraext.TTF", 24);

	//Loading Textures
	ResourcesManager::LoadTexture("texture/mine.png","ball", GL_TRUE);
	ResourcesManager::LoadTexture("texture/paddle.png", "paddle", GL_TRUE);

	ResourcesManager::LoadTexture("texture/cloud.png", "particule", GL_TRUE);
	ResourcesManager::LoadTexture("texture/block.png", "block", GL_FALSE);
	ResourcesManager::LoadTexture("texture/block_solid.png", "block_solid", GL_FALSE);
	ResourcesManager::LoadTexture("texture/background.jpg", "background", GL_FALSE);

	//Loading PowerUps Texture
	ResourcesManager::LoadTexture("texture/powerup/powerup_chaos.png", "powerup_chaos", GL_FALSE);
	ResourcesManager::LoadTexture("texture/powerup/powerup_confuse.png", "powerup_confuse", GL_FALSE);
	ResourcesManager::LoadTexture("texture/powerup/powerup_increase.png", "powerup_increase", GL_FALSE);
	ResourcesManager::LoadTexture("texture/powerup/powerup_passthrough.png", "powerup_passthrough", GL_FALSE);
	ResourcesManager::LoadTexture("texture/powerup/powerup_speed.png", "powerup_speed", GL_FALSE);
	ResourcesManager::LoadTexture("texture/powerup/powerup_sticky.png", "powerup_sticky", GL_FALSE);
	//Loading levels
	levels.push_back(GameLevel("level/BounceGalore.lvl", width, height*heightFactor));
	levels.push_back(GameLevel("level/SmallGaps.lvl", width, height*heightFactor));
	levels.push_back(GameLevel("level/SpaceInvader.lvl", width, height*heightFactor));
	levels.push_back(GameLevel("level/Standard.lvl", width, height*heightFactor));
	levels.push_back(GameLevel("level/Heart.lvl", width, height*heightFactor));
	currentLevel = 2;

	//instancing a postProcessing : (a  multisampled framebuffer)
	Effects = new PostProcessor(ResourcesManager::getShaderByName("PostProcess") , width , height);
	glm::vec2 paddlePos = glm::vec2(width / 2 - PADDLE_SIZE.x / 2, height - PADDLE_SIZE.y);
	//instancing a SpiritRenderer
	renderer = new SpriteRenderer(ResourcesManager::getShaderByName("spriteShader"));
	bkRenderer = new SpriteRenderer(ResourcesManager::getShaderByName("Heart"));
	//instancing a particule generator
	ParticlesGenerator = new ParticleGenerator(ResourcesManager::getShaderByName("Particule"), ResourcesManager::getTextureByName("particule"), NR_PARTICULE);
	//instancing a paddle
	paddle = new GameObject(paddlePos, PADDLE_SIZE, ResourcesManager::getTextureByName("paddle"));
	//instancing a ball
	glm::vec2 ballPos = paddlePos + glm::vec2(PADDLE_SIZE.x / 2 - BALL_RADIUS, -BALL_RADIUS *2);
	Ball = new BallObject(ballPos, BALL_RADIUS, INITIAL_BALL_VELOCITY, ResourcesManager::getTextureByName("ball"));

	SoundEngine->play2D("audio/breakout.mp3", GL_TRUE);
}

void Game::render(){
	
	GLfloat time = glfwGetTime();

	if (state == GAME_ACTIVE)
	{
		Effects->BeginRender();		
		// Draw background
		bkRenderer->DrawSprite(time, vec2(width, height));
		//renderer->DrawSprite(ResourcesManager::getTextureByName("background"),glm::vec2(0, 0), glm::vec2(width,height), 0.0f);	
		paddle->Draw(*renderer);		
		// Draw level
		levels[currentLevel].Draw(*renderer);

		if (Ball->PassThrough){
			ParticlesGenerator->Draw();
		}
		
		Ball->Draw(*renderer);

		for (PowerUp &powerUp : this->PowerUps)
			if (!powerUp.Destroyed)
				powerUp.Draw(*renderer);

		Text->RenderText("Lives:", 5.0f, 5.0f, 1.0f);
		Effects->EndRender();
		Effects->Render(time);			
	}
}

void Game::ProcessInput(GLfloat dt, GLFWwindow* window){
	if (state == GAME_ACTIVE)
	{
		GLfloat velocity = PADDLE_VELOCITY * dt;

		if (glfwGetKey(window, GLFW_KEY_Q) || glfwGetKey(window, GLFW_KEY_LEFT))
		{
			if (paddle->Position.x >= 0){
				paddle->Position.x -= velocity;
				if (Ball->Stuck)
					Ball->Position.x -= velocity;
			}			
		}
		if (glfwGetKey(window, GLFW_KEY_D) || glfwGetKey(window, GLFW_KEY_RIGHT))
		{
			if (paddle->Position.x <= width - paddle->Size.x){
				paddle->Position.x += velocity;
				if (Ball->Stuck)
					Ball->Position.x += velocity;
			}				
		}

		if (glfwGetKey(window, GLFW_KEY_SPACE))
			Ball->Stuck = false;
	}
}


void Game::ResetLevel()
{
	//reset current level
	//levels[currentLevel].resetLevel(width,height * heightFactor);
	// Reset player/ball stats
	Effects->Reset();
	PowerUps.clear();
	paddle->Size = PADDLE_SIZE;
	paddle->Position = glm::vec2(width / 2 - PADDLE_SIZE.x / 2, height - PADDLE_SIZE.y);
	Ball->Reset(paddle->Position + glm::vec2(PADDLE_SIZE.x / 2 - BALL_RADIUS, -(BALL_RADIUS * 2)), INITIAL_BALL_VELOCITY);
}


void Game::Update(GLfloat dt)
{
	
	Ball->Move(dt, width);

	if (!Ball->Stuck)
	{
		BallCollision();
		
	}

	if (Ball->Position.y >= height) // Did ball reach bottom edge?
	{
		this->ResetLevel();
	}

	if (Ball->PassThrough)
	{
		ParticlesGenerator->Update(dt, *Ball, NR_NEW_PARTICULE, glm::vec2(Ball->Radius / 2));
	}
	

	if (ShakeTime > 0.0f)
	{
		ShakeTime -= dt;
		if (ShakeTime <= 0.0f)
			Effects->Shake = false;
	}

	UpdatePowerUps(dt);
}

void ActivatePowerUp(PowerUp &powerUp)
{
	if (powerUp.Type == "speed")
	{
		Ball->Velocity *= 1.2;
	}
	else if (powerUp.Type == "sticky")
	{
		Ball->Sticky = GL_TRUE;
		paddle->Color = glm::vec3(1.0f, 0.5f, 1.0f);
	}
	else if (powerUp.Type == "pass-through")
	{
		Ball->PassThrough = GL_TRUE;
		Ball->Color = glm::vec3(1.0f, 0.5f, 0.5f);
	}
	else if (powerUp.Type == "pad-size-increase")
	{
		paddle->Size.x += 50;
	}
	else if (powerUp.Type == "confuse")
	{
		if (!Effects->Chaos)
			Effects->Confuse = GL_TRUE; // Only activate if chaos wasn't already active
	}
	else if (powerUp.Type == "chaos")
	{
		if (!Effects->Confuse)
			Effects->Chaos = GL_TRUE;
	}
}

void Game::BallCollision(){
	ResolvePaddleCollision(Ball->checkCollision(*paddle));
	for (GameObject &box : levels[currentLevel].Bricks)
	{
		if (!box.Destroyed)
		{
			Collision collision = Ball->checkCollision(box);
			if (get<0>(collision)){
				if (!box.IsSolid)
				{
					box.Destroyed = GL_TRUE;
					SpawnPowerUps(box);
				}
				else{
					ShakeTime = SHAKE_TIME;
					Effects->Shake = true;
				}
				if (!(Ball->PassThrough && !box.IsSolid))
					ResolveBrickCollision(collision);	
			}
		}
	}

	for (PowerUp &powerUp : PowerUps)
	{
		if (!powerUp.Destroyed)
		{
			if (powerUp.Position.y >= height)
				powerUp.Destroyed = GL_TRUE;
			if (paddle->checkAABBCollision(powerUp))
			{	// Collided with player, now activate powerup
				ActivatePowerUp(powerUp);
				powerUp.Destroyed = GL_TRUE;
				powerUp.Activated = GL_TRUE;
			}
		}
	}
}

//we DID not check the left or right edges of the paddle ????

void Game::ResolvePaddleCollision(Collision collision){
	if (!Ball->Stuck  && std::get<0>(collision))
	{
		Ball->Stuck = Ball->Sticky;
		// Check where it hit the board, and change velocity based on where it hit the board
		GLfloat centerBoard = paddle->Position.x + paddle->Size.x / 2;
		GLfloat distance = (Ball->Position.x + Ball->Radius) - centerBoard;
		GLfloat percentage = distance / (paddle->Size.x / 2);
		// Then move accordingly
		GLfloat strength = 2.0f;
		glm::vec2 oldVelocity = Ball->Velocity;
		Ball->Velocity.x = INITIAL_BALL_VELOCITY.x * percentage * strength;
		//Ball->Velocity.y = -Ball->Velocity.y;
		Ball->Velocity = glm::normalize(Ball->Velocity) * glm::length(oldVelocity); // Keep speed consistent over both axes (multiply by length of old velocity, so total strength is not changed)
		// Fix sticky paddle
		Ball->Velocity.y = -1 * abs(Ball->Velocity.y);
	}
}


void Game::ResolveBrickCollision(Collision collision){

	// Collision resolution
	Direction dir = std::get<1>(collision);
	glm::vec2 diff_vector = std::get<2>(collision);
	if (dir == LEFT || dir == RIGHT) // Horizontal collision
	{
		Ball->Velocity.x = -Ball->Velocity.x; // Reverse horizontal velocity
		// Relocate
		GLfloat penetration = Ball->Radius - std::abs(diff_vector.x);
		if (dir == LEFT)
			Ball->Position.x += penetration; // Move ball to right
		else
			Ball->Position.x -= penetration; // Move ball to left;
	}
	else // Vertical collision
	{
		Ball->Velocity.y = -Ball->Velocity.y; // Reverse vertical velocity
		// Relocate
		GLfloat penetration = Ball->Radius - std::abs(diff_vector.y);
		if (dir == UP)
			Ball->Position.y -= penetration; // Move ball bback up
		else
			Ball->Position.y += penetration; // Move ball back down
	}
}



void Game::UpdatePowerUps(GLfloat dt){
	for (PowerUp &powerUp : this->PowerUps)
	{
		powerUp.Position += powerUp.Velocity * dt;
		if (powerUp.Activated)
		{
			powerUp.Duration -= dt;

			if (powerUp.Duration <= 0.0f)
			{
				// Remove powerup from list (will later be removed)
				powerUp.Activated = GL_FALSE;
				// Deactivate effects
				if (powerUp.Type == "sticky")
				{
					if (!isOtherPowerUpActive(this->PowerUps, "sticky"))
					{	// Only reset if no other PowerUp of type sticky is active
						Ball->Sticky = GL_FALSE;
						paddle->Color = glm::vec3(1.0f);
					}
				}
				else if (powerUp.Type == "pass-through")
				{
					if (!isOtherPowerUpActive(this->PowerUps, "pass-through"))
					{	// Only reset if no other PowerUp of type pass-through is active
						Ball->PassThrough = GL_FALSE;
						Ball->Color = glm::vec3(1.0f);
					}
				}
				else if (powerUp.Type == "confuse")
				{
					if (!isOtherPowerUpActive(this->PowerUps, "confuse"))
					{	// Only reset if no other PowerUp of type confuse is active
						Effects->Confuse = GL_FALSE;
					}
				}
				else if (powerUp.Type == "chaos")
				{
					if (!isOtherPowerUpActive(this->PowerUps, "chaos"))
					{	// Only reset if no other PowerUp of type chaos is active
						Effects->Chaos = GL_FALSE;
					}
				}
			}
		}
	}
	this->PowerUps.erase(std::remove_if(this->PowerUps.begin(), this->PowerUps.end(),
		[](const PowerUp &powerUp) { return powerUp.Destroyed && !powerUp.Activated; }
	), this->PowerUps.end());
}
void Game::SpawnPowerUps(GameObject& block){
	if (ShouldSpawn(75)) // 1 in 75 chance
		this->PowerUps.push_back(
		PowerUp("speed", glm::vec3(0.5f, 0.5f, 1.0f), 0.0f, block.Position, ResourcesManager::getTextureByName("powerup_speed")
		));
	if (ShouldSpawn(75))
		this->PowerUps.push_back(
		PowerUp("sticky", glm::vec3(1.0f, 0.5f, 1.0f), 20.0f, block.Position, ResourcesManager::getTextureByName("powerup_sticky")
		));
	if (ShouldSpawn(75))
		this->PowerUps.push_back(
		PowerUp("pass-through", glm::vec3(0.5f, 1.0f, 0.5f), 10.0f, block.Position, ResourcesManager::getTextureByName("powerup_passthrough")
		));
	if (ShouldSpawn(75))
		this->PowerUps.push_back(
		PowerUp("pad-size-increase", glm::vec3(1.0f, 0.6f, 0.4), 0.0f, block.Position, ResourcesManager::getTextureByName("powerup_increase")
		));
	if (ShouldSpawn(15)) // Negative powerups should spawn more often
		this->PowerUps.push_back(
		PowerUp("confuse", glm::vec3(1.0f, 0.3f, 0.3f), 12.0f, block.Position, ResourcesManager::getTextureByName("powerup_confuse")
		));
	if (ShouldSpawn(15))
		this->PowerUps.push_back(
		PowerUp("chaos", glm::vec3(0.9f, 0.25f, 0.25f), 12.0f, block.Position, ResourcesManager::getTextureByName("powerup_chaos")
		));
}


GLboolean Game::ShouldSpawn(GLuint chance)
{
	GLuint random = rand() % chance;
	return random == 0;
}

GLboolean Game::isOtherPowerUpActive(std::vector<PowerUp> &powerUps, std::string type)
{
	for (const PowerUp &powerUp : powerUps)
	{
		if (powerUp.Activated)
		if (powerUp.Type == type)
			return GL_TRUE;
	}
	return GL_FALSE;
}