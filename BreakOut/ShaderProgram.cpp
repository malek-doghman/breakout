#include "ShaderProgram.h"
#include "glException.h"

ShaderProgram::ShaderProgram() :program_id(0){

}

GLuint ShaderProgram::getProgramId()
{
	return(program_id);
}

ShaderProgram& ShaderProgram::use(){
	glUseProgram(program_id);
	return *this;
}

GLint ShaderProgram::findUniformLocation(string name){
	GLint id = glGetUniformLocation(program_id, name.c_str());

	if (id < 0)
	{
		cout << "uniform location not found for this name *" << name<< std::endl;
	}

	return id;
	
}

void ShaderProgram::sendUniformData(string uniformName, int value){
	GLint uLocation = findUniformLocation(uniformName);
	glUniform1i(uLocation, value);
}

void ShaderProgram::sendUniformData(string uniformName, GLuint value){
	GLint uLocation = findUniformLocation(uniformName);
	glUniform1i(uLocation, value);
}

void ShaderProgram::sendUniformData(string uniformName, GLfloat value){
	GLint uLocation = findUniformLocation(uniformName);
	glUniform1f(uLocation, value);
}

void ShaderProgram::sendUniformData(string uniformName, GLdouble value){
	GLint uLocation = findUniformLocation(uniformName);
	glUniform1f(uLocation, static_cast<float>(value));
}

void ShaderProgram::sendUniformData(string uniformName, vec2& v){
	GLint uLocation = findUniformLocation(uniformName);
	glUniform2fv(uLocation, 1, &v[0]);
}

void ShaderProgram::sendUniformData(string uniformName, vec3& v){
	GLint uLocation = findUniformLocation(uniformName);
	glUniform3fv(uLocation, 1, &v[0]);
}

void ShaderProgram::sendUniformData(string uniformName, vec4& v){
	GLint uLocation = findUniformLocation(uniformName);
	glUniform4fv(uLocation, 1, &v[0]);
}

void ShaderProgram::sendUniformData(string uniformName, mat3& m){
	GLint uLocation = findUniformLocation(uniformName);
	glUniformMatrix3fv(uLocation, 1, GL_FALSE, &m[0][0]);
}

void ShaderProgram::sendUniformData(string uniformName, mat4& m){
	GLint uLocation = findUniformLocation(uniformName);
	glUniformMatrix4fv(uLocation, 1, GL_FALSE, &m[0][0]);
}

GLuint ShaderProgram::build(const GLchar *vertex, const GLchar *fragment, const GLchar *geometry){
	insertShader(GL_VERTEX_SHADER,vertex);
	insertShader(GL_FRAGMENT_SHADER,fragment);
	
	if (geometry != nullptr)
	{
		insertShader(GL_GEOMETRY_SHADER ,geometry);
	}

	buildProgram();
	return program_id;
}

void ShaderProgram::insertShader(GLenum shaderType, string shaderName){
	shader tmp;
	tmp.filename = shaderName;
	tmp.shaderType = shaderType;
	shaderArray.push_back(tmp);
}

void ShaderProgram::compileShaders()
{
	for (shader shader_iter : shaderArray)
	{
		GLuint newShader = glCreateShader(shader_iter.shaderType);
		string src = readFile(shader_iter.filename);
		const char* pointer = &src[0];
		int size = src.size();
		glShaderSource(newShader, 1, &pointer, &size);
		glCompileShader(newShader);
		GLint compileStatus;
		glGetShaderiv(newShader, GL_COMPILE_STATUS, &compileStatus);
		if (compileStatus == GL_FALSE){
			GLint loglength;
			glGetShaderiv(newShader, GL_INFO_LOG_LENGTH, &loglength);
			string log;
			log.resize(loglength);
			glGetShaderInfoLog(newShader, loglength, &loglength, &log[0]);
			throw(glException(log));
		}
		compiledShaders.push_back(newShader);

	}
	shaderArray.clear();

}

void ShaderProgram::buildProgram(){
	compileShaders();
	program_id = glCreateProgram();
	for (GLuint shader : compiledShaders){
		glAttachShader(program_id, shader);
		glDeleteShader(shader);
	}
	glLinkProgram(program_id);
	compiledShaders.clear();
	GLint linkStatus;
	glGetProgramiv(program_id, GL_LINK_STATUS, &linkStatus);
	if (linkStatus == GL_FALSE){
		int loglength;
		glGetProgramiv(program_id, GL_INFO_LOG_LENGTH, &loglength);
		string log;
		log.resize(loglength);
		glGetProgramInfoLog(program_id, loglength, &loglength, &log[0]);
		throw(glException(log));

	}
}


string ShaderProgram::readFile(string filename)
{
	string output, tmp;
	std::fstream shaderFile;
	shaderFile.open(&filename[0], std::ios::in);
	if (shaderFile.is_open()){
		while (std::getline(shaderFile, tmp))
		{
			output += tmp + "\n";
		}
		shaderFile.close();
		return(output);
	}
	else{
		output = filename + " is not found !";
		throw(glException(output));
	}
}

