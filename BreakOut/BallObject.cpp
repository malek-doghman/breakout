#include "BallObject.h"

BallObject::BallObject()
: GameObject(), Radius(12.5f){
	Stuck = GL_TRUE; PassThrough = GL_FALSE;  Sticky = GL_FALSE; 
}

BallObject::BallObject(glm::vec2 pos, GLfloat radius, glm::vec2 velocity, Texture2D sprite)
: GameObject(pos, glm::vec2(radius * 2, radius * 2), sprite, glm::vec3(1.0f), velocity), Radius(radius), Stuck(true) {
	Stuck = GL_TRUE; PassThrough = GL_FALSE;  Sticky = GL_FALSE;
}

glm::vec2 BallObject::Move(GLfloat dt, GLuint window_width)
{
	// If not stuck to player board
	if (!this->Stuck)
	{
		// Move the ball
		this->Position += this->Velocity * dt;
		// Then check if outside window bounds and if so, reverse velocity and restore at correct position
		if (this->Position.x <= 0.0f)
		{
			this->Velocity.x = -this->Velocity.x;
			this->Position.x = 0.0f;
		}
		else if (this->Position.x + this->Size.x >= window_width)
		{
			this->Velocity.x = -this->Velocity.x;
			this->Position.x = window_width - this->Size.x;
		}
		if (this->Position.y <= 0.0f)
		{
			this->Velocity.y = -this->Velocity.y;
			this->Position.y = 0.0f;
		}
	}
	return this->Position;
}

// Resets the ball to initial Stuck Position (if ball is outside window bounds)
void BallObject::Reset(glm::vec2 position, glm::vec2 velocity)
{
	Position = position;
	Velocity = velocity;
	Stuck = GL_TRUE;
	Sticky = GL_FALSE;
	PassThrough = GL_FALSE;
	Color = glm::vec3(1.0f);
}


//WARNING !!!!!!!!!!!!!!!!!!!!!!!!!!!!
//WHEN THE BALL IS INSIDE THE BOX ,THE CLOSESTpoint will be the same as the ball center this will result in 
//  a null difference vector and then in a runtime error when executiing the vector direction function
//WARNING !!!!!!!!!!!!!!!!!!!!!!!!!!!!!

Collision BallObject::checkCollision(GameObject& box){

	glm::vec2 BallCenter(Position.x + Radius, Position.y + Radius);
	glm::vec2 halfSize(box.Size.x / 2, box.Size.y / 2);
	glm::vec2 BoxCenter(box.Position.x + halfSize.x , box.Position.y + halfSize.y);

	glm::vec2 diff = BallCenter - BoxCenter; 
	glm::vec2 clampDiff = clamp(diff, -halfSize, halfSize);
	glm::vec2 closestPoint = BoxCenter + clampDiff;

	glm::vec2 difference = closestPoint - BallCenter;

	if (glm::length(difference) < Radius && glm::length(difference) > 0){
		return std::make_tuple(GL_TRUE, VectorDirection(difference), difference);
	}
	else
		return std::make_tuple(GL_FALSE, UP, glm::vec2(0, 0));


}

Direction BallObject::VectorDirection(glm::vec2 target)
{
	glm::vec2 compass[] = {
		glm::vec2(0.0f, 1.0f),	// up
		glm::vec2(1.0f, 0.0f),	// right
		glm::vec2(0.0f, -1.0f),	// down
		glm::vec2(-1.0f, 0.0f)	// left
	};
	GLfloat max = 0.0f;
	GLuint best_match = -1;
	for (GLuint i = 0; i < 4; i++)
	{
		GLfloat dot_product = glm::dot(glm::normalize(target), compass[i]);
		if (dot_product > max)
		{
			max = dot_product;
			best_match = i;
		}
	}
	return (Direction)best_match;
}