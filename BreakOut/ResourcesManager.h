#pragma once 
#include "Common.h"
#include "texture.h"
#include "ShaderProgram.h"


class ResourcesManager
{
public:

	~ResourcesManager();
	static Texture2D getTextureByName(string name);
	static ShaderProgram getShaderByName(string name);

	void static LoadShader(string name, const GLchar *vertex, const GLchar *fragment, const GLchar *geometry = nullptr);
	void static LoadTexture(string path , string name , GLboolean alpha);

	static void Clear();

private:
	ResourcesManager();
	static Texture2D loadTextureFromFile(string path, GLboolean alpha);
	static map<string, Texture2D> textureMap;
	static map<string, ShaderProgram> shaderMap;
};

