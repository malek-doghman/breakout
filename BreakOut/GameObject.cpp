#include "GameObject.h"

GameObject::GameObject()
: Position(0, 0), Size(1, 1), Velocity(0.0f), Color(1.0f), Rotation(0.0f), Sprite(), IsSolid(false), Destroyed(false) { }

GameObject::GameObject(glm::vec2 pos, glm::vec2 size, Texture2D sprite, glm::vec3 color, glm::vec2 velocity)
: Position(pos), Size(size), Velocity(velocity), Color(color), Rotation(0.0f), Sprite(sprite), IsSolid(false), Destroyed(false) { }

void GameObject::Draw(SpriteRenderer &renderer)
{
	renderer.DrawSprite(this->Sprite, this->Position, this->Size, this->Rotation, this->Color);
}

GLboolean GameObject::checkAABBCollision(GameObject& box){
	bool collisionX = this->Position.x + this->Size.x >= box.Position.x &&
		box.Position.x + box.Size.x >= this->Position.x;
	// Collision y-axis?
	bool collisionY = this->Position.y + this->Size.y >= box.Position.y &&
		box.Position.y + box.Size.y >= this->Position.y;
	// Collision only if on both axes
	return collisionX && collisionY;
}