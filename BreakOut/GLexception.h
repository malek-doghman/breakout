#pragma once
#include "Common.h"

using std::runtime_error;

class glException : public runtime_error
{
public:
	glException();
	glException(std::string);
	//glException(const glException&);

};