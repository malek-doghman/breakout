#pragma once
#include "Common.h"
#include "TextureChar.h"
#include "ShaderProgram.h"

class TextRenderer
{
public:
	TextRenderer(ShaderProgram& textRenderer , int faceWidth = 0 , int faceHeight = 144 );
	~TextRenderer();
	void Load();
	void RenderText(std::string text, GLfloat x, GLfloat y, GLfloat scale, glm::vec3 color);

private:
	void InitFT();
	void InitFace();	
	void LoadTextureChars();
	void LoadChar(char glyph);

	ShaderProgram _textShader;
	GLuint VAO, VBO;

	FT_Library ft;
	FT_Face face;
	std::map<GLchar, TextureChar> Characters;

	string fontDirectory = "font/OpenSans-Light.ttf";
	int _faceWidth ;
	int _faceHeight;

	string FTLoadError = "ERROR::FREETYPE: Could not init FreeType Library";
	string FaceLoadError = "ERROR::FREETYPE: Failed to load font"; 
	string GlyphLoadError = "ERROR::FREETYTPE: Failed to load Glyph";
};
