
#include "GameLevel.h"


glm::vec3 processColor(int indice);

GameLevel::GameLevel(const GLchar *file, GLuint levelWidth, GLuint levelHeight){
	Load(file,levelWidth,levelHeight);
}

void GameLevel::resetLevel(GLuint levelWidth, GLuint levelHeight){
	init(levelWidth,levelHeight);
}
void GameLevel::Load(const GLchar *file, GLuint levelWidth, GLuint levelHeight)
{
	// Clear old data
	
	// Load from file
	GLuint tileCode;
	//GameLevel level;
	std::string line;
	std::ifstream fstream(file);
	//fstream.open(file);
	if (fstream)
	{
		while (std::getline(fstream, line)) // Read each line from level file
		{
			std::istringstream sstream(line);
			std::vector<GLuint> row;
			while (sstream >> tileCode) // Read each word seperated by spaces
				row.push_back(tileCode);
			tileData.push_back(row);
		}
		if (tileData.size() > 0)
			this->init(levelWidth, levelHeight);
	}
}

void GameLevel::Draw(SpriteRenderer &renderer)
{
	for (GameObject &tile : this->Bricks)
	if (!tile.Destroyed)
		tile.Draw(renderer);
}

GLboolean GameLevel::IsCompleted()
{
	for (GameObject &tile : this->Bricks)
	if (!tile.IsSolid && !tile.Destroyed)
		return GL_FALSE;
	return GL_TRUE;
}

void GameLevel::init(GLuint levelWidth, GLuint levelHeight)
{
	//OPTIMIZATION :DDD
	tileSize.x = tileData[0].size();
	tileSize.y = tileData.size();

	this->Bricks.clear();

	// Calculate dimensions
	GLuint height = tileData.size();
	GLuint width = tileData[0].size(); // Note we can index vector at [0] since this function is only called if height > 0
	GLfloat unit_width = levelWidth / static_cast<GLfloat>(width), unit_height = levelHeight / height;
	// Initialize level tiles based on tileData		
	for (GLuint y = 0; y < height; ++y)
	{
		for (GLuint x = 0; x < width; ++x)
		{
			glm::vec2 pos(unit_width * x, unit_height * y);
			glm::vec2 size(unit_width, unit_height);
			// Check block type from level data (2D level array)
			if (tileData[y][x] == 0)
			{
				GameObject obj(pos, size, ResourcesManager::getTextureByName("block"), vec3(0.0f));
				obj.Destroyed = GL_TRUE;
				this->Bricks.push_back(obj);
			}
			else
			if (tileData[y][x] == 1) // Solid
			{
				GameObject obj(pos, size, ResourcesManager::getTextureByName("block_solid"), processColor(tileData[y][x]));
				obj.IsSolid = GL_TRUE;
				this->Bricks.push_back(obj);
			}
			else if (tileData[y][x] > 1)	// Non-solid; now determine its color based on level data
			{
				this->Bricks.push_back(GameObject(pos, size, ResourcesManager::getTextureByName("block"), processColor(tileData[y][x])));
			}
		}
	}
}

glm::vec3 processColor(int indice){

	vec3 PURPLE = vec3(0.678f, 0.078f, 0.341f);
	vec3 StrongPink = vec3(0.761f,0.094f,0.35f);
	vec3 VividPink = vec3(0.914f, 0.118f, 0.388f);
	vec3 BrightPink = vec3(0.925f, 0.251f, 0.478f);
	vec3 SoftPink = vec3(0.941f, 0.384f, 0.573f);
	vec3 VerySoftPink = vec3(0.973f, 0.733f, 0.816f);

	vec3 Orange = vec3(1.0f, 0.5f, 0.0f);
	vec3 GreenOlive = vec3(0.8f, 0.8f, 0.4f);
	vec3 Green = vec3(0.0f, 0.7f, 0.0f);
	vec3 Blue = vec3(0.2f, 0.6f, 1.0f);

	vec3 White = vec3(0.8f, 0.8f, 0.7f);

	switch (indice)
	{
	case 2 :
		return GreenOlive;
	case 3:
		return Green; 
	case 4:
		return Blue;
	case 5:
		return Orange;
	case 6:
		return PURPLE;
	case 7 : 
		return StrongPink;
	case 8 :
		return VividPink;
	case 9 : 
		return SoftPink;
	default:
		return White;
	}
}