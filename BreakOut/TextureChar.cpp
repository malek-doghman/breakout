#include "TextureChar.h"

TextureChar::TextureChar()
{
	Texture2D();
	Internal_Format = GL_RED;
	Wrap_S = GL_CLAMP_TO_EDGE;
	Wrap_T = GL_CLAMP_TO_EDGE;
}

TextureChar::~TextureChar()
{
}

void TextureChar::Generate(FT_Face face){

	Texture2D::Generate(face->glyph->bitmap.width,
						face->glyph->bitmap.rows, 
						face->glyph->bitmap.buffer);

	_BearingX = face->glyph->bitmap_left;
	_BearingY = face->glyph->bitmap_top;
	_Advance = face->glyph->advance.x ;
}

