#pragma once
#include "Common.h"
#include "GLexception.h"

class Window
{
public:
	Window(std::string _title, int _width, int  _height, bool fullscreen = false, int _samplesNumber = 0, float red = 0.0f, float green = 0.0f, float blue = 0.0f);
	~Window();

	GLFWwindow* GetWindow();
	int getWidth();
	int getHeight();
	std::vector<float> getBackGroundColor();
	void render();
private:
	void glfwInitialize();
	void glInitialize();

	std::string title; 
	int width; 
	int height; 
	int samplesNumber; 
	std::vector<float> backgroundColor;
	bool Fullscreen;
	GLFWwindow* window;
	
};

