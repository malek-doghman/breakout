#include "Window.h"

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
	if (key == GLFW_KEY_ESCAPE || key == GLFW_KEY_F1 && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);
}



GLFWwindow* Window::GetWindow(){
	return window; 
}

int Window::getHeight(){
	return height; 
}

int Window::getWidth(){
	return width; 
}

vector<float> Window::getBackGroundColor(){
	return backgroundColor;
}

Window::Window(std::string _title, int _width, int  _height, bool fullscreen, int _samplesNumber, float red, float green, float blue)
{
	title = _title; 
	width = _width; 
	height = _height;
	samplesNumber = _samplesNumber;
	Fullscreen = fullscreen;
	backgroundColor.push_back(red);
	backgroundColor.push_back(green);
	backgroundColor.push_back(blue);

	glfwInitialize();
	glInitialize();
}

Window::~Window()
{

}


void Window::glfwInitialize(){
	if (!glfwInit()){
		throw(glException("GLFW initialization error"));
	}
	else{
		//MSAA anti aliasing 
		if (samplesNumber > 0)
		{
			glfwWindowHint(GLFW_SAMPLES, samplesNumber);
		}
		if (Fullscreen)
		{
			window = glfwCreateWindow(width, height, &title[0], glfwGetPrimaryMonitor(), 0);
		}
		else
			window = glfwCreateWindow(width, height, &title[0], 0, 0);

		//glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
		//glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
		//glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
		//glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

	}

}

void Window::glInitialize(){

	glfwMakeContextCurrent(window);
	if (glewInit() != 0){
		throw(glException("GL initialization error"));
	}

	glViewport(0, 0, width, height);
	glfwSetKeyCallback(window, key_callback);
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);


	glEnable(GL_BLEND);
	//this is the blend default function
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	//glEnable(GL_DEPTH_TEST);
	//glDepthFunc(GL_LESS);

	//glEnable(GL_PROGRAM_POINT_SIZE);
	//glEnable(GL_MULTISAMPLE);
	//glEnable(GL_CULL_FACE);

	//glEnable(GL_STENCIL_TEST);
	//glStencilFunc(GL_NOTEQUAL, 1, 0xFF);
	//glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);

	
	
	
}

void Window::render(){

	while (!glfwWindowShouldClose(window))
	{
		glClearBufferfv(GL_COLOR, 0, &backgroundColor[0]);
		glfwPollEvents();
		glfwSwapBuffers(window);
	}

	glfwTerminate();
}