#pragma once 
#include "Common.h"
#include "texture.h"

class TextureChar : public Texture2D
{
public:
	TextureChar();
	~TextureChar();
	void Generate(FT_Face face);
	int _BearingX;
	int _BearingY;
	GLuint _Advance;
};

