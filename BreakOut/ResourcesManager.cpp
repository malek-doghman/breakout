#include "ResourcesManager.h"

ResourcesManager::ResourcesManager()
{
}

ResourcesManager::~ResourcesManager()
{
}

std::map<std::string, Texture2D>     ResourcesManager::textureMap;
std::map<std::string, ShaderProgram> ResourcesManager::shaderMap;

void ResourcesManager::Clear(){

	for (auto iter : shaderMap)
		glDeleteProgram(iter.second.getProgramId());

	for (auto iter : textureMap)
		glDeleteTextures(1, &iter.second.ID);
}

Texture2D ResourcesManager::getTextureByName(string name){
	return textureMap[name];
}

ShaderProgram ResourcesManager::getShaderByName(string name){
	return shaderMap[name];
}

void ResourcesManager::LoadShader(string name , const GLchar *vertex, const GLchar *fragment, const GLchar *geometry){
	
	ShaderProgram shader;
	shader.build(vertex, fragment, geometry);
	shaderMap[name] = shader; 
}

void ResourcesManager::LoadTexture(string path, string name, GLboolean alpha){
	textureMap[name] = loadTextureFromFile(path, alpha);
}

Texture2D ResourcesManager::loadTextureFromFile(string path, GLboolean alpha){

	Texture2D texture;
	if (alpha)
	{
		texture.Internal_Format = GL_RGBA;
		texture.Image_Format = GL_RGBA;
	}
	// Load image
	int width, height;
	unsigned char* image = SOIL_load_image(&path[0], &width, &height, 0, texture.Image_Format == GL_RGBA ? SOIL_LOAD_RGBA : SOIL_LOAD_RGB);
	// Now generate texture
	texture.Generate(width, height, image);
	// And finally free image data
	SOIL_free_image_data(image);
	return texture;
}
