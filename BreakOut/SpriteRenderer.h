#pragma once 
#include "Common.h"
#include "ShaderProgram.h"
#include "texture.h"

class SpriteRenderer
{
public:
	SpriteRenderer(ShaderProgram &_shader);
	~SpriteRenderer();
	void DrawSprite(GLfloat time , glm::vec2 size, glm::vec2 position = vec2(0.0f), GLfloat rotate = 0.0f);
	void DrawSprite(Texture2D &texture, glm::vec2 position, glm::vec2 size = glm::vec2(10, 10), GLfloat rotate = 0.0f, glm::vec3 color = glm::vec3(1.0f));
private:
	GLuint quadVertexArrayObject;
	ShaderProgram shader;
	void InitRenderData();
};
