#version 330 core

out vec4 color;
in vec2 texCoords;
uniform vec3 spriteColor;
uniform sampler2D spriteTexture;
void main(){
	color = texture(spriteTexture , texCoords) * vec4(spriteColor , 1.0f) ; 
}