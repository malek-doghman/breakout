// License Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.
// https://www.shadertoy.com/view/MdBGDK
// By David Hoskins.

#version 400

uniform vec2 iResolution ; 
uniform float iGlobalTime ; 

void main( void )
{
	float f = 3., g = 3.;
	vec2 res = iResolution.xy;
	vec2 mou = vec2(0.0,0.0);

		mou.x = sin(iGlobalTime * .3)*sin(iGlobalTime * .17) * 1. + sin(iGlobalTime * .3);
		mou.y = (1.0-cos(iGlobalTime * .632))*sin(iGlobalTime * .131)*1.0+cos(iGlobalTime * .3);
		mou = (mou+1.0) * res;
        
	vec2 z = ((-res+2.0 * gl_FragCoord.xy) / res.y);
	vec2 p = ((-res+2.0+mou) / res.y);
	for( int i = 0; i < 20; i++) 
	{
		float d = dot(z,z);
		z = (vec2( z.x, -z.y ) / d) + p; 
		z.x =  abs(z.x);
		f = max( f, (dot(z-p,z-p) ));
		g = min( g, sin(dot(z+p,z+p))+1.0);
	}
	f = abs(-log(f) / 3.5);
	g = abs(-log(g) / 8.0);
	gl_FragColor = vec4(min(vec3(g, g*f, f), 1.0),1.0);
}
