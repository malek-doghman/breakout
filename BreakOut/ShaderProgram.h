#pragma once
#include "Common.h"
using namespace glm;

struct shader{
	GLenum shaderType;
	string filename;
};

class ShaderProgram{
public:
	ShaderProgram();
	void buildProgram();
	void insertShader(GLenum shaderType, string shaderName);
	GLuint getProgramId();
	GLuint build(const GLchar *vertex, const GLchar *fragment, const GLchar *geometry = nullptr);
	//ShaderProgram(const ShaderProgram &);
	ShaderProgram& use();

	GLint findUniformLocation(string name);

	void sendUniformData(string, GLfloat);
	void sendUniformData(string, GLdouble);
	void sendUniformData(string, GLint);
	void sendUniformData(string uniformName, GLuint value);

	void sendUniformData(string, vec2&);
	void sendUniformData(string, vec3&);
	void sendUniformData(string, vec4&);

	void sendUniformData(string, mat3&);
	void sendUniformData(string, mat4&);

private:	
	void compileShaders();
	string readFile(string);
	vector <shader> shaderArray;
	vector <GLuint> compiledShaders;
	GLuint program_id;
};